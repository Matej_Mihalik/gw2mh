****************************************

      GW2 Monster Hunter

****************************************

This is a Javascript app designed for all the boss hunters in the world of Tyria. It features world boss timers, as well as a convenient logbook functionality, enabling you to log which bosses you already defeated on any given day. The stored information is persistent, so you can close and open the app as many times as you like, your data will stay right where they are. That is, until the time of the GW2 daily reset (00:00 UTC) when the logbook is automatically reset, no need for manual interaction at all!

This app was primarily designed as an Overwolf (http://www.overwolf.com) app, but it should work in any modern browser as well (use 'main.html' entry point).



----------------------------------------

                 About

----------------------------------------

The app features several views:

- WORLD BOSSES - features all 13 world bosses of Tyria, each displaying the countdown to the closest world boss event occurence. The boss entries are sorted, with closest ones are on the top, while bosses that are defeated for the day stay at the bottom of the list.

- WORLD BOSS ROTATION - presents a complete view of daily world boss rotation, from the present moment up to 24 hours ahead.

- SETTINGS & INFO - enables users to set a few preferences and view when does the daily reset occurs.

Why another GW2 boss timers app? Most of the existing apps, which were using the official Guild Wars 2 API (http://wiki.guildwars2.com/wiki/API:Main), broke with the release of GW2 April 2014 Feature Pack, when megaservers were introduced and world boss events got synchronized between servers. The API hasn't been updated to reflect those changes so far, and the only reliable info on the world boss timers comes in form of a pdf (https://d3b4yo2b5lbfy.cloudfront.net/wp-content/uploads/2014/06/0e65eWorld-Event-Times-EN-6-17-2014.pdf).

Also, none of the existing timer apps featured logbook functionality in a way I would like, so I decided to make my own app and release it to public in the hope that will also be useful to others.

Besides, being able to fire this app straight from Overwolf kicks ass! :)



----------------------------------------

                License

----------------------------------------

Guild Wars 2 Monster Hunter
Copyright (C) 2014 Matej Mihalik <matej.mihalik.sk@gmail.com>
Licensed under the GNU Affero General Public License
See <http://www.gnu.org/licenses/agpl-3.0.html> or LICENSE.txt for details

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.



----------------------------------------

          Third party licenses

----------------------------------------

jQuery
Copyright (C) 2005, 2014 jQuery Foundation, Inc.
Licensed under the MIT License
See <https://jquery.org/license/> or LICENSE_JQUERY.txt for details

Moment.js
Copyright (C) 2011-2014 Tim Wood, Iskren Chernev, Moment.js contributors
Licensed under the MIT License
See <http://momentjs.com> or LICENSE_MOMENTS.txt for details

PT Serif
Copyright (C) 2009 ParaType Ltd.
with Reserved Names 'PT Sans' and 'ParaType'
Licensed under the Paratype PT Sans Free Font License
See <http://www.fontsquirrel.com/license/pt-serif> or LICENSE_FONT.txt for details



----------------------------------------

                Contact

----------------------------------------

Author: <matej.mihalik.sk@gmail.com>
Code repository: <https://bitbucket.org/Matej_Mihalik/gw2mh>
