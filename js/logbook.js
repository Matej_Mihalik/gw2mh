// LOGBOOK
function Logbook() {
	this.logs = {};
	this.currentLog = null;
	this.intervalID = 0;
	this.updateLock = 0;
	this.dailyResetTime = moment.utc(globalDailyReset, "HH:mm").local();
}

Logbook.prototype.setInterval = function() {
	var logbook = this;
	this.intervalID = setInterval(function() {
		logbook.update();
	}, 1000);
};

Logbook.prototype.clearInterval = function() {
	clearInterval(this.intervalID);
};

Logbook.prototype.reset = function() {
	globalDataStorage.resetDaily(this.dailyResetTime);
	$.each(this.logs, function(name, log) {
		log.reset();
	});
};

Logbook.prototype.update = function() {
	if(this.updateLock>0)
		return true;

	globalDataStorage.refreshSettings();
	globalDataStorage.refreshBossData();
	globalCurrentTime = moment();

	if(globalCurrentTime.isAfter(this.dailyResetTime))
		this.dailyResetTime.add(1, 'd');
	if(globalCurrentTime.isAfter(parseInt(globalDataStorage.getDailyResetTime())))
		this.reset();

	this.currentLog.update();
};

Logbook.prototype.changeTimeFormat = function() {
	$.each(this.logs, function(name, log) {
		log.changeTimeFormat();
	});
};

Logbook.prototype.changeTrackedBosses = function() {
	this.updateLock++;
	$.each(this.logs, function(name, log) {
		log.changeTrackedBosses();
	});
	this.updateLock--;
};

Logbook.prototype.switchLog = function(log) {
	this.clearInterval();

	if(this.currentLog !== null)
		this.currentLog.hide();
	this.currentLog = this.logs[log];
	this.currentLog.show();
	this.currentLog.update();

	this.setInterval();
};

Logbook.prototype.bind = function() {
	// dummy
};

function MainLogbook() {
	Logbook.call(this);
	this.logs = {
		'boss':new BossLog(),
		'rotation':new RotationLog(),
		'settings':new SettingsLog()
	};

	this.bind();
	this.switchLog(globalDataStorage.getOption('default-tab'));
}

MainLogbook.prototype = new Logbook();

MainLogbook.prototype.constructor = MainLogbook;

MainLogbook.prototype.bind = function() {
	$('.log-header').bind('click', function() {
		if($(this).hasClass('log-header-selected'))
			return true;

		if($(this).attr('data-type')==='resize')
			windowSwitch('SubWindow');
		else
			globalLogbook.switchLog($(this).attr('data-type'));
	});
};

function SubLogbook() {
	Logbook.call(this);
	this.logs = {
		'miniboss':new MiniBossLog()
	};

	this.bind();
	this.switchLog('miniboss');
}

SubLogbook.prototype = new Logbook();

SubLogbook.prototype.constructor = SubLogbook;

SubLogbook.prototype.bind = function() {
	$('.resize-grip').bind('mousedown', function() {
		windowResize($(this).attr('data-type'));
	});

	$('#Logbook').bind('mousedown', function() {
		windowMove();
	});

	$('.log-header[data-type="resize"]').bind('click',function() {
		windowSwitch('MainWindow');
	});

	$('.log-header[data-type="close"]').bind('click',function() {
		windowClose();
	});
};

// LOGS
function Log() {
	this.title = '';
	this.header = null;
	this.body = null;
	this.entries = [];
}

Log.prototype.hide = function() {
	this.header.removeClass('log-header-selected');
	this.body.hide();
};

Log.prototype.show = function() {
	this.header.addClass('log-header-selected');
	this.body.show();
};

Log.prototype.sort = function() {
	// dummy
};

Log.prototype.draw = function() {
	// dummy
};

Log.prototype.update = function() {
	this.redraw();
	this.resort();
};

Log.prototype.animate = function(element) {
	// dummy
};

Log.prototype.redraw = function() {
	// dummy
};

Log.prototype.resort = function() {
	// dummy
};

Log.prototype.empty = function() {
	this.body.empty();
	for(var i=0; i<this.entries.length; i++) {
		this.entries[i].element = null;
	}
};

Log.prototype.changeTimeFormat = function() {
	for(var i=0; i<this.entries.length; i++) {
		var entry = this.entries[i];
		if(entry.element!==null)
			entry.changeTimeFormat();
	}
};

Log.prototype.changeTrackedBosses = function() {
	// dummy
};

Log.prototype.reset = function() {
	for(var i=0; i<this.entries.length; i++) {
		var entry = this.entries[i];
		if(entry.element!==null)
			entry.reset();
	}
};

Log.prototype.bind = function() {
	this.body.find('.boss-entry-check').bind('click', function() {
		if(globalLogbook.updateLock>0)
			return true;

		globalLogbook.updateLock++;

		var element = $(this);
		var type = element.parent().attr('data-type');

		$('.boss-entry[data-type="'+type+'"]').children('.boss-entry-check').toggleClass('boss-entry-check-checked');
		globalDataStorage.setBossData(type, element.hasClass('boss-entry-check-checked'));

		globalLogbook.currentLog.animate(element.parent());
	});

	this.body.find('.boss-entry-code-input').bind('click', function() {
		this.select();
	});

	this.body.find('.settings-entry-options-input').bind('change', function() {
		var type = $(this).parents('.settings-entry').attr('data-type');

		if(type==='tracked-bosses') {
			var value = {};
			$(this).parent().parent().children().each(function() {
				value[$(this).attr('data-type')]=$(this).children('input[type="checkbox"]').prop('checked');
			});
			globalDataStorage.setOption(type, value);
		}
		else
			globalDataStorage.setOption(type, $(this).val());

		if(type==='time-format')
			globalLogbook.changeTimeFormat();
		else if(type==='tracked-bosses')
			globalLogbook.changeTrackedBosses();
	});
};

function BossLog() {
	Log.call(this);
	this.title = 'World Bosses';
	this.header = $('.log-header[data-type="boss"]').text(this.title);
	this.body = $('.log-body[data-type="boss"]');

	var log = this;
	var bossTimes = {};
	for(var i=0; i<globalBossTimes.length; i++) {
		var entry = globalBossTimes[i];
		var boss = entry.id;
		var time = entry.time;

		if(boss in bossTimes)
			bossTimes[boss].push(time);
		else
			bossTimes[boss] = [time];
	}
	$.each(bossTimes, function(boss, times) {
		log.entries.push(new BossLogEntry(boss, times));
	});

	this.sort();
	this.draw();
}

BossLog.prototype = new Log();

BossLog.prototype.constructor = BossLog;

BossLog.prototype.sort = function() {
	this.entries.sort(function(a, b) {
		var checkedA = globalDataStorage.getBossData(a.boss.id);
		var checkedB = globalDataStorage.getBossData(b.boss.id);
		var timeA = a.times[0];
		var timeB = b.times[0];
		var diffA = a.boss.diff;
		var diffB = b.boss.diff;

		if(checkedA !== checkedB)
			return checkedA ? 1 : -1;
		else if(!timeA.isSame(timeB))
			return timeA.isAfter(timeB) ? 1 : -1;
		else
			return diffA>diffB ? 1 : -1;
	});
};

BossLog.prototype.draw = function() {
	for(var i=0; i<this.entries.length; i++) {
		var entry = this.entries[i];
		if(globalDataStorage.getOption('tracked-bosses')[entry.boss.id])
			entry.insert(this.body);
	}
	this.bind();
};

BossLog.prototype.animate = function(element) {
	this.sort();

	var currentIndex = element.index();
	var newIndex = currentIndex;
	var entries = $.grep(this.entries, function(entry) {
		return entry.element!==null;
	});

	for(var i=0; i<entries.length; i++) {
		if(element.get(0)===entries[i].element.get(0)) {
			newIndex = i;
			break;
		}
	}

	var height = element.outerHeight();
	var min = currentIndex+1;
	var max = newIndex+1;
	var log = this;
	if(currentIndex>newIndex) {
		min = newIndex;
		max = currentIndex;
		height *= -1;
	}

	element.animate({top: this.body.children().eq(newIndex).position().top - element.position().top}, 500);
	this.body.children().slice(min, max).animate({top:-height}, 500);
	this.body.children().promise().done(function() {
		log.body.children().css('top', '');
		globalLogbook.updateLock--;
		globalLogbook.update();
	});
};

BossLog.prototype.redraw = function() {
	for(var i=0; i<this.entries.length; i++) {
		var entry = this.entries[i];
		entry.timeShift();
		if(entry.element!==null)
			entry.update();
	}
};

BossLog.prototype.resort = function() {
	this.sort();
	var entries = $.grep(this.entries, function(entry) {
		return entry.element!==null;
	});

	for(var i=0; i<entries.length; i++) {
		var element = entries[i].element;
		var newIndex = i;
		var currentIndex = element.index();
		if(currentIndex !== newIndex)
			this.body.children().eq(newIndex).after(element.detach());
	}
};

BossLog.prototype.changeTrackedBosses = function() {
	this.empty();
	this.draw();
};

function RotationLog() {
	Log.call(this);
	this.title = 'World Boss Rotation';
	this.header = $('.log-header[data-type="rotation"]').text(this.title);
	this.body = $('.log-body[data-type="rotation"]');

	var log = this;
	for(var i=0; i<globalBossTimes.length; i++) {
		var entry = globalBossTimes[i];
		log.entries.push(new RotationLogEntry(entry.id, entry.time));
	}

	this.sort();
	this.draw();
}

RotationLog.prototype = new Log();

RotationLog.prototype.constructor = RotationLog;

RotationLog.prototype.sort = function() {
	var min = Number.MAX_VALUE;
	var first = null;

	for(var i=0; i<this.entries.length; i++) {
		var entry = this.entries[i];
		var diff = entry.time.diff(globalCurrentTime);
		if(diff>0 && diff<min) {
			min = diff;
			first = entry;
		}
	}

	while(this.entries[0]!==first) {
		this.entries.push(this.entries.shift());
	}
};

RotationLog.prototype.draw = function() {
	for(var i=0; i<this.entries.length; i++) {
		var entry = this.entries[i];
		if(globalDataStorage.getOption('tracked-bosses')[entry.boss.id])
			entry.insert(this.body);
	}
	this.bind();
};

RotationLog.prototype.animate = function(element) {
	globalLogbook.updateLock--;
	globalLogbook.update();
};

RotationLog.prototype.redraw = function() {
	for(var i=0; i<this.entries.length; i++) {
		var entry = this.entries[i];
		entry.timeShift();
		if(entry.element!==null)
			entry.update();
	}
};

RotationLog.prototype.resort = function() {
	var min = Number.MAX_VALUE;
	var first = null;

	for(var i=0; i<this.entries.length; i++) {
		var entry = this.entries[i];
		var diff = entry.time.diff(globalCurrentTime);
		if(diff>0 && diff<min) {
			min = diff;
			first = entry;
		}
	}

	while(this.entries[0]!==first) {
		var element = this.entries[0].element;
		if(element!==null)
			this.body.append(this.entries[0].element.detach());
		this.entries.push(this.entries.shift());
	}
};

RotationLog.prototype.changeTrackedBosses = function() {
	this.empty();
	this.draw();
};

function SettingsLog() {
	Log.call(this);
	this.title = 'Settings & Info';
	this.header = $('.log-header[data-type="settings"]');
	this.body = $('.log-body[data-type="settings"]');

	this.entries.push(new SettingsLogEntry(globalDailyReset));

	this.draw();
}

SettingsLog.prototype = new Log();

SettingsLog.prototype.constructor = SettingsLog;

SettingsLog.prototype.draw = function() {
	for(var i=0; i<this.entries.length; i++) {
		this.entries[i].insert(this.body);
	}
	this.drawOptions();
	this.bind();
};

SettingsLog.prototype.drawOptions = function() {
	var bossCount = 0;
	var selectArrow = $('<img class="select-arrow" src="img/select.png">');
	var timeFormatInput = $('<select class="settings-entry-options-input"><option value="hh:mm a">12-hour</option><option value="HH:mm">24-hour</option></select>');
	var defaultTabInput = $('<select class="settings-entry-options-input"><option value="boss">World Bosses</option><option value="rotation">World Boss Rotation</option><option value="settings">Settings & Info</option></select>');
	var bossLimitInput = $('<select class="settings-entry-options-input">');
	var trackedBossesInput = $('<div>');
	$.each(globalBossData, function(key, value) {
		bossCount++;
		$('<option>').attr('value', bossCount).text(bossCount).appendTo(bossLimitInput);

		var innerElement =  $('<div class="settings-entry-option">').attr('data-type', value.id).appendTo(trackedBossesInput);
		$('<input type="checkbox" class="settings-entry-options-input">').prop('checked', globalDataStorage.getOption('tracked-bosses')[key]).appendTo(innerElement);
		$('<label>').text(value.name).appendTo(innerElement);
	});

	timeFormatInput.val(globalDataStorage.getOption('time-format'));
	defaultTabInput.val(globalDataStorage.getOption('default-tab'));
	bossLimitInput.val(globalDataStorage.getOption('boss-limit'));

	var element = $('<div class="settings-entry">').attr('data-type', 'time-format');
	$('<div class="settings-entry-name">').text('Time format').appendTo(element);
	$('<div class="settings-entry-options">').append(timeFormatInput).append(selectArrow.clone()).appendTo(element);
	element.appendTo(this.body);

	element = $('<div class="settings-entry">').attr('data-type', 'default-tab');
	$('<div class="settings-entry-name">').text('Default tab').appendTo(element);
	$('<div class="settings-entry-options">').append(defaultTabInput).append(selectArrow.clone()).appendTo(element);
	element.appendTo(this.body);

	element = $('<div class="settings-entry">').attr('data-type', 'boss-limit');
	$('<div class="settings-entry-name">').text('Minimized boss limit').appendTo(element);
	$('<div class="settings-entry-options">').append(bossLimitInput).append(selectArrow.clone()).appendTo(element);
	element.appendTo(this.body);

	element = $('<div class="settings-entry">').attr('data-type', 'tracked-bosses');
	$('<div class="settings-entry-name">').text('Tracked bosses').appendTo(element);
	$('<div class="settings-entry-options">').append(trackedBossesInput.children()).appendTo(element);
	element.appendTo(this.body);
};

SettingsLog.prototype.redraw = function() {
	for(var i=0; i<this.entries.length; i++) {
		var entry = this.entries[i];
		entry.timeShift();
		if(entry.element!==null)
			entry.update();
	}
};

function MiniBossLog() {
	Log.call(this);
	this.title = 'World Bosses';
	this.header = $('.log-header[data-type="miniboss"]').text(this.title);
	this.body = $('.log-body[data-type="miniboss"]');

	var log = this;
	var bossTimes = {};
	for(var i=0; i<globalBossTimes.length; i++) {
		var entry = globalBossTimes[i];
		var boss = entry.id;
		var time = entry.time;

		if(boss in bossTimes)
			bossTimes[boss].push(time);
		else
			bossTimes[boss] = [time];
	}
	$.each(bossTimes, function(boss, times) {
		log.entries.push(new MiniBossLogEntry(boss, times));
	});

	this.sort();
	this.draw();
}

MiniBossLog.prototype = new Log();

MiniBossLog.prototype.constructor = MiniBossLog;

MiniBossLog.prototype.sort = function() {
	this.entries.sort(function(a, b) {
		var checkedA = globalDataStorage.getBossData(a.boss.id);
		var checkedB = globalDataStorage.getBossData(b.boss.id);
		var timeA = a.times[0];
		var timeB = b.times[0];
		var diffA = a.boss.diff;
		var diffB = b.boss.diff;

		if(checkedA !== checkedB)
			return checkedA ? 1 : -1;
		else if(!timeA.isSame(timeB))
			return timeA.isAfter(timeB) ? 1 : -1;
		else
			return diffA>diffB ? 1 : -1;
	});
};

MiniBossLog.prototype.draw = function() {
	var entryLimit = globalDataStorage.getOption('boss-limit');
	var entries = $.grep(this.entries, function(entry) {
		return globalDataStorage.getOption('tracked-bosses')[entry.boss.id] && !globalDataStorage.getBossData(entry.boss.id);
	});

	for(var i=0; i<entryLimit && i<entries.length; i++) {
		entries[i].insert(this.body);
	}
	this.bind();
};

MiniBossLog.prototype.animate = function(element) {
	this.sort();

	element.animate({'opacity':0}, 500, function() {
		$(this).css({'display':'none', 'opacity':''});
		globalLogbook.updateLock--;
		globalLogbook.update();
	});
};

MiniBossLog.prototype.redraw = function() {
	for(var i=0; i<this.entries.length; i++) {
		var entry = this.entries[i];
		entry.timeShift();
	}
};

MiniBossLog.prototype.resort = function() {
	this.sort();

	this.empty();
	this.draw();

	this.bind();
};



// ENTRIES
function LogEntry() {
	this.boss = null;
	this.time = null;
	this.times = [];
	this.element = null;
}

LogEntry.prototype.timeShift = function() {
	// dummy
};

LogEntry.prototype.insert = function() {
	return null;
};

LogEntry.prototype.update = function() {
	// dummy
};

LogEntry.prototype.changeTimeFormat = function() {
	// dummy
};

LogEntry.prototype.reset = function() {
	// dummy
};

function BossLogEntry(boss, times) {
	LogEntry.call(this);
	this.boss = globalBossData[boss];
	this.times = [];
	for(var i=0; i<times.length; i++) {
		this.times.push(moment.utc(times[i], "HH:mm").local());
	}

	this.timeShift();
}

BossLogEntry.prototype = new LogEntry();

BossLogEntry.prototype.constructor = BossLogEntry;

BossLogEntry.prototype.timeShift = function() {
	while(this.times[0].isBefore(globalCurrentTime)) {
		this.times[0].add(1, 'd');
		this.times.push(this.times.shift());
	}
};

BossLogEntry.prototype.insert = function(body) {
	var codeInput = $('<input type="text" class="boss-entry-code-input" readonly="readonly">');
	codeInput.val(this.boss['wp-code']);

	var element = $('<div class="boss-entry">').attr('data-type', this.boss.id);
	$('<div class="boss-entry-diff">').addClass('boss-entry-diff-'+this.boss.diff).appendTo(element);
	$('<div class="boss-entry-check">').toggleClass('boss-entry-check-checked', globalDataStorage.getBossData(this.boss.id)).appendTo(element);
	$('<div class="boss-entry-icon">').addClass('boss-entry-icon-'+this.boss.id).appendTo(element);
	$('<div class="boss-entry-name">').text(this.boss.name).appendTo(element);
	$('<div class="boss-entry-until">').text(moment.utc(this.times[0].diff(globalCurrentTime)).format('HH:mm:ss')).appendTo(element);
	$('<div class="boss-entry-time">').text(this.times[0].format(globalDataStorage.getOption('time-format'))).appendTo(element);
	$('<div class="boss-entry-code">').append(codeInput).appendTo(element);
	this.element = element.appendTo(body);

	return this.element;
};

BossLogEntry.prototype.update = function() {
	this.element.find('.boss-entry-check').toggleClass('boss-entry-check-checked', globalDataStorage.getBossData(this.boss.id));
	this.element.find('.boss-entry-until').text(moment.utc(this.times[0].diff(globalCurrentTime)).format('HH:mm:ss'));
	this.element.find('.boss-entry-time').text(this.times[0].format(globalDataStorage.getOption('time-format')));
};

BossLogEntry.prototype.changeTimeFormat = function() {
	this.element.find('.boss-entry-time').text(this.times[0].format(globalDataStorage.getOption('time-format')));
};

BossLogEntry.prototype.reset = function() {
	this.element.find('.boss-entry-check').removeClass('boss-entry-check-checked');
};

function RotationLogEntry(boss, time) {
	LogEntry.call(this);
	this.boss = globalBossData[boss];
	this.time = moment.utc(time, "HH:mm").local();

	this.timeShift();
}

RotationLogEntry.prototype = new LogEntry();

RotationLogEntry.prototype.constructor = RotationLogEntry;

RotationLogEntry.prototype.timeShift = function() {
	if(this.time.isBefore(globalCurrentTime))
		this.time.add(1, 'd');
};

RotationLogEntry.prototype.insert = function(body) {
	var codeInput = $('<input type="text" class="boss-entry-code-input" readonly="readonly">');
	codeInput.val(this.boss['wp-code']);

	var element = $('<div class="boss-entry">').attr('data-type', this.boss.id);
	$('<div class="boss-entry-diff">').addClass('boss-entry-diff-'+this.boss.diff).appendTo(element);
	$('<div class="boss-entry-check">').toggleClass('boss-entry-check-checked', globalDataStorage.getBossData(this.boss.id)).appendTo(element);
	$('<div class="boss-entry-icon">').addClass('boss-entry-icon-'+this.boss.id).appendTo(element);
	$('<div class="boss-entry-name">').text(this.boss.name).appendTo(element);
	$('<div class="boss-entry-until">').text(moment.utc(this.time.diff(globalCurrentTime)).format('HH:mm:ss')).appendTo(element);
	$('<div class="boss-entry-time">').text(this.time.format(globalDataStorage.getOption('time-format'))).appendTo(element);
	$('<div class="boss-entry-code">').append(codeInput).appendTo(element);
	this.element = element.appendTo(body);

	return this.element;
};

RotationLogEntry.prototype.update = function() {
	this.element.find('.boss-entry-check').toggleClass('boss-entry-check-checked', globalDataStorage.getBossData(this.boss.id));
	this.element.find('.boss-entry-until').text(moment.utc(this.time.diff(globalCurrentTime)).format('HH:mm:ss'));
};

RotationLogEntry.prototype.changeTimeFormat = function() {
	this.element.find('.boss-entry-time').text(this.time.format(globalDataStorage.getOption('time-format')));
};

RotationLogEntry.prototype.reset = function() {
	this.element.find('.boss-entry-check').removeClass('boss-entry-check-checked');
};

function SettingsLogEntry(time) {
	LogEntry.call(this);
	this.time = moment.utc(time, "HH:mm").local();

	this.timeShift();
}

SettingsLogEntry.prototype = new LogEntry();

SettingsLogEntry.prototype.constructor = RotationLogEntry;

SettingsLogEntry.prototype.timeShift = function() {
	if(this.time.isBefore(globalCurrentTime))
		this.time.add(1, 'd');
};

SettingsLogEntry.prototype.insert = function(body) {
	var element = $('<div class="reset-entry">');
	$('<div class="reset-entry-name">').text('Daily reset').appendTo(element);
	$('<div class="reset-entry-until">').text(moment.utc(this.time.diff(globalCurrentTime)).format('HH:mm:ss')).appendTo(element);
	$('<div class="reset-entry-time">').text(this.time.format(globalDataStorage.getOption('time-format'))).appendTo(element);
	this.element = element.appendTo(body);

	return this.element;
};

SettingsLogEntry.prototype.update = function() {
	this.element.find('.reset-entry-until').text(moment.utc(this.time.diff(globalCurrentTime)).format('HH:mm:ss'));
};

SettingsLogEntry.prototype.changeTimeFormat = function() {
	this.element.find('.reset-entry-time').text(this.time.format(globalDataStorage.getOption('time-format')));
};

function MiniBossLogEntry(boss, times) {
	LogEntry.call(this);
	this.boss = globalBossData[boss];
	this.times = [];
	for(var i=0; i<times.length; i++) {
		this.times.push(moment.utc(times[i], "HH:mm").local());
	}

	this.timeShift();
}

MiniBossLogEntry.prototype = new LogEntry();

MiniBossLogEntry.prototype.constructor = MiniBossLogEntry;

MiniBossLogEntry.prototype.timeShift = function() {
	while(this.times[0].isBefore(globalCurrentTime)) {
		this.times[0].add(1, 'd');
		this.times.push(this.times.shift());
	}
};

MiniBossLogEntry.prototype.insert = function(body) {
	var element = $('<div class="boss-entry">').attr('data-type', this.boss.id);
	$('<div class="boss-entry-check">').toggleClass('boss-entry-check-checked', globalDataStorage.getBossData(this.boss.id)).appendTo(element);
	$('<div class="boss-entry-name">').text(this.boss.name).appendTo(element);
	$('<div class="boss-entry-until">').text(moment.utc(this.times[0].diff(globalCurrentTime)).format('HH:mm:ss')).appendTo(element);
	this.element = element.appendTo(body);

	return this.element;
};

MiniBossLogEntry.prototype.update = function() {
	this.element.find('.boss-entry-check').toggleClass('boss-entry-check-checked', globalDataStorage.getBossData(this.boss.id));
	this.element.find('.boss-entry-until').text(moment.utc(this.times[0].diff(globalCurrentTime)).format('HH:mm:ss'));
};

MiniBossLogEntry.prototype.reset = function() {
	this.element.find('.boss-entry-check').removeClass('boss-entry-check-checked');
};
