var globalCurrentTime = null;
var globalDataStorage = null;
var globalLogbook = null;

$(document).ready(function() {
	var windowType = $('#Logbook').attr('data-type');

	if(windowType==='MainWindow')
		initMainWindow();
	else if(windowType==='SubWindow')
		initSubWindow();
});

function initMainWindow() {
	globalCurrentTime = moment();
	globalDataStorage = new DataStorage();
	globalLogbook = new MainLogbook();
}

function initSubWindow() {
	globalCurrentTime = moment();
	globalDataStorage = new DataStorage();
	globalLogbook = new SubLogbook();
}

function windowResize(edge) {
	if(typeof overwolf==='undefined')
		return true;

	overwolf.windows.getCurrentWindow(function(result) {
		if(result.status==="success")
			overwolf.windows.dragResize(result.window.id, edge);
	});
}

function windowMove() {
	if(typeof overwolf==='undefined')
		return true;

	overwolf.windows.getCurrentWindow(function(result) {
		if(result.status==='success')
			overwolf.windows.dragMove(result.window.id);
	});
}

function windowSwitch(windowName) {
	if(typeof overwolf==='undefined')
		return true;

	overwolf.windows.obtainDeclaredWindow(windowName, function(result) {
		if(result.status==='success') {
			overwolf.windows.restore(result.window.id, function(result) {
				if(result.status==='success') {
					overwolf.windows.getCurrentWindow(function(result) {
						if(result.status==='success')
							overwolf.windows.minimize(result.window.id, function(result) {});
					});
				}
			});
		}
	});
}

function windowClose() {
	if(typeof overwolf==='undefined')
		return true;

	overwolf.windows.getCurrentWindow(function(result) {
		if(result.status==='success')
			overwolf.windows.close(result.window.id, function(result) {});
	});
}

Array.prototype.move = function(from, to) {
	this.splice(to, 0, this.splice(from, 1)[0]);
};
