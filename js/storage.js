function DataStorage() {
	this.dailyResetTime = 0;

	this.bossData = {};

	this.settings = {
		'time-format': 'HH:mm',
		'default-tab': 'boss',
		'boss-limit': 3,
		'tracked-bosses': {}
	};

	var storage = this;
	$.each(globalBossData, function(key, value) {
		storage.bossData[key] = false;
		storage.settings['tracked-bosses'][key] = true;
	});

	this.refreshDailyResetTime();
	this.refreshBossData();
	this.refreshSettings();
}

DataStorage.prototype.refreshDailyResetTime = function() {
	var scheduledReset = localStorage.getItem('gw2mh-dailyresettime');
	if(scheduledReset!==null)
		this.dailyResetTime = scheduledReset;
};

DataStorage.prototype.refreshBossData = function() {
	$.extend(this.bossData, JSON.parse(localStorage.getItem('gw2mh-bossdata')));
};

DataStorage.prototype.refreshSettings = function() {
	$.extend(this.settings, JSON.parse(localStorage.getItem('gw2mh-settings')));
};

DataStorage.prototype.getDailyResetTime = function() {
	return this.dailyResetTime;
};

DataStorage.prototype.setDailyResetTime = function(time) {
	this.dailyResetTime = time.valueOf();
	localStorage.setItem('gw2mh-dailyresettime', this.dailyResetTime);
};

DataStorage.prototype.getBossData = function(bossID) {
	return this.bossData[bossID];
};

DataStorage.prototype.setBossData = function(bossID, status) {
	this.bossData[bossID] = status;
	localStorage.setItem('gw2mh-bossdata', JSON.stringify(this.bossData));
};

DataStorage.prototype.getOption = function(key) {
	return this.settings[key];
};

DataStorage.prototype.setOption = function(key, value) {
	this.settings[key] = value;
	localStorage.setItem('gw2mh-settings', JSON.stringify(this.settings));
};

DataStorage.prototype.resetDaily = function(time) {
	var storage = this;
	$.each(storage.bossData, function(key, value) {
		storage.bossData[key] = false;
	});
	localStorage.setItem('gw2mh-bossdata', JSON.stringify(this.bossData));
	this.setDailyResetTime(time);
};
